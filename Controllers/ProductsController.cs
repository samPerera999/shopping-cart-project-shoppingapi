﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingApi.Models;

namespace ShoppingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductsController : ControllerBase
    {
        private readonly ShoppingApiContext context;

        public ProductsController(ShoppingApiContext context)
        {
            this.context = context;
        }

        //GET /api/products
        public async Task<ActionResult<IEnumerable<Product>>> Get()
        {
            return await context.Products.OrderBy(x => x.Id).ToListAsync();
        }

        //GET /api/products/category
        [HttpGet("{categoryName}")]
        public async Task<ActionResult<IEnumerable<Product>>> GetByCategory(string categoryName)
        {
            var category = context.Categories.SingleOrDefault(c => c.Slug == categoryName);
            if (category != null)
            {
                return await context.Products.Where(p => p.CategoryId == category.Id).OrderBy(x => x.Id).ToListAsync(); 
            }
            return NotFound();
        }
    }
}