﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using ShoppingApi.Models;

namespace ShoppingApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PagesController : ControllerBase
    {
        private readonly ShoppingApiContext context;

        public PagesController(ShoppingApiContext context)
        {
            this.context = context;
        }

        public async Task<ActionResult<IEnumerable<Page>>> Get()
        {
            return await context.Pages.OrderBy(x => x.Id).ToListAsync();
        }

        //public IEnumerable<Page> Get()
        //{
        //    return context.Pages.OrderBy(x => x.Id).ToList();
        //}
    }
}